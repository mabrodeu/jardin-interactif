/*

Le code suivant est basé sur le code de Anastasis Chasandras, sur openprocessing.
Le code a été modifié entre autre pour avoir la notion de vent, à partir des données de l'Arduino.
Le visuel des éclaboussure et le système de génération de la pluie (dans le sketch principal) a été modifié.

License originale:

"Rain" by Anastasis Chasandras, licensed under Creative Commons Attribution-Share Alike 3.0 and GNU GPL license.
Work: http://openprocessing.org/visuals/?visualID=9299 
License: 
http://creativecommons.org/licenses/by-sa/3.0/
http://creativecommons.org/licenses/GPL/2.0/

*/

public class Rain
{
  PVector position,pposition,speed;
  float col;
   
  public Rain()
  {
    position = new PVector(random(-width,width*2),-100);
    pposition = position;
    speed = new PVector(0,0);
    col = random(30,100);
  }
   
  void draw()
  {
  	pushStyle();

  		colorMode(HSB,100);
	    stroke(100,col);
	    strokeWeight(3);
	    line(position.x,position.y,pposition.x,pposition.y);
    popStyle();
  }
   
  void calculate()
  {
    pposition = new PVector(position.x,position.y);
    gravity();
 
  }
   
  void gravity()
  {
    speed.y += .2;
    float newX = constrain(windAngle/1.5, 0, 10);
    speed.x = newX;  

    position.add(speed);
  }
}
 
public class Splash
{
  PVector position,speed;
   
  public Splash(float x,float y)
  {
    float angle = random(PI,TWO_PI);
    float distance = random(1,5);
    float xx = cos(angle)*distance;
    float yy = sin(angle)*distance;
    position = new PVector(x,y);
    speed = new PVector(xx,yy);
     
  }
   
  public void draw()
  {
  	pushStyle();
		colorMode(HSB,100);
	    strokeWeight(3);
	    stroke(100,50);
	    fill(100,100);
	    ellipse(position.x,position.y,2,2);
    popStyle();
  }
   
  void calculate()
  {
    gravity();
      
    speed.x*=0.98;
    speed.y*=0.98;
            
    position.add(speed);
  }
   
  void gravity()
  {
    speed.y+=.2;
  }
   
}
