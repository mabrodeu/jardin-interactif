/**
* Project : jardin_interactif 
* Author : marcantoinebrodeur & pierolivierbourgault
* Version : 1.0.0
* Date : 29-10-2014
* 
* En appuyant sur la touche 1, un timer de 5 secondes permet de calibrer l'anémomètre
* la touche 2 calibre la photo résistance
* la touche 3 calibre l'eau
*/

import oscP5.*;
import netP5.*;
import processing.serial.*;
import java.util.Iterator;
import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

OscP5 oscP5;
NetAddress remoteLocationOSC;
OscMessage oscMessage;

// Saisir les bons noms de ports pour les 2 Arduinos ici
String portName = "/dev/cu.usbmodem1451";
String portName2 = "/dev/cu.usbmodem1411";
// =====================================================

Serial serial;
Serial serial2;

String messageFirstElement = "";
int messageSecondElement = 0;

boolean ready = false;

ArrayList<Branch> treeArray = new ArrayList<Branch>();
Branch tree;
float windAngle = 0;
float minX;
float maxX;
float minY;
float maxY;

float calibrateMillisWind = 0.0;
float valueWind = 0;
float calibMinWind = 1023;
float calibMaxWind = 0;
float defaultWindSpeed = 0.0;
float defaultWindDirection = 0.0;
float lopWind;
float previousLopWind = 0;
float lopWindAmount = 0.005;
int windDirection = 1;
boolean recalibrateWind = true;
float timeBetweenWindValues = 0.0;

float calibrateMillisPhoto = 0.0;
float valuePhoto = 0;
float calibMinPhoto = 1023;
float calibMaxPhoto = 0;
float lopPhoto;
float previousLopPhoto = 0;
float lopPhotoAmount = 0.05;
float photoTargetMaxVal = 255;
float photoValReal = 0;
boolean recalibratePhoto = true;

float calibrateMillisEau = 0.0;
float valueEau = 0;
float calibMinEau = 1023;
float calibMaxEau = 0;
float eauTargetMaxVal = 4000;
float tresholdEau = eauTargetMaxVal*0.75;
boolean recalibrateEau = true;

int valueTerre = 0;

color backgroundColor = 0;
float backgroundColorHue = 180;
boolean doCreateNewTree = false;

PImage bgDegrade;

// on refresh le vent actuel à toutes les 10 minutes
TimeoutP5 getCurrentWindTimer = new TimeoutP5(600000);

int rainNum = 100;
ArrayList rain = new ArrayList();
ArrayList splash = new ArrayList();
float current;
float reseed = random(0,.2);
float maxRain = 0;
boolean activateRain = false;

void setup() {

	size(1280, 729, P3D);
    frameRate(60);

	noCursor();

    // au chargement et à chaque 10 minutes, on va chercher le vent actuel à l'emplactement où le sketch
    // a été lancé pour obtenir la direction du vent actuel. Permet d'avoir un sketch différent selon la météo actuelle
	getCurrentWind();

    // initialisation OSC
	oscP5 = new OscP5(this,12346);
	remoteLocationOSC = new NetAddress("127.0.0.1",12346);

	try {
		serial = new Serial(this, portName, 57600);
		serial2 = new Serial(this, portName2, 57600);
	} catch (Exception e) {
		println("error serial");
	}	

    // Indiquer a l'instance serial de lancer la fonction serialEvent()
    // lorsque l'octet 13 est recu. L'octet 13 est envoye par
    // l'Arduino pour indiquer la fin du message
    serial.bufferUntil(13);
    serial2.bufferUntil(13);

    bgDegrade = loadImage("bg_degrade.png");

  	current = millis();

    noSmooth();

    Ani.init(this);
}

void draw() {

	if(getCurrentWindTimer.isFinished()){
		getCurrentWind();
		getCurrentWindTimer.start();
	}

    if( millis() - timeBetweenWindValues > 400 ){
        valueWind = 0;
        timeBetweenWindValues = millis();

        int samples = 10;
        lopWind = lopWind + ((valueWind - lopWind)/samples) + defaultWindSpeed;
        sendOSCMessage("/wind", lopWind);

        sendOSCMessage("/windmax", calibMaxWind);
    }

    background(backgroundColor);

    image(bgDegrade, 0, 0, width, height);

    ready = true;

    if(ready){
	    if(defaultWindDirection > 0.0 && defaultWindDirection < 180.0){
	    	windDirection = 1;
	    } else if(defaultWindDirection >= 180.0 && defaultWindDirection < 360.0){
	    	windDirection = -1;
	    }

	    //windAngle = windDirection*(defaultWindSpeed+lopWind);
        windAngle = lopWind;

	    pushStyle();
    		colorMode(HSB, 360, 100, photoTargetMaxVal);
    		backgroundColor = color(backgroundColorHue, 100, photoValReal);
	    popStyle();
    }

    if(doCreateNewTree){
    	boolean noTreeNear = false;
    	doCreateNewTree = false;
    	
    	float posX = random(width/6, width-width/6);
    	int compteurLoop = 0;

    	if(treeArray.size() > 0){
	    	while(!noTreeNear){

	    		posX = random(width/6, width-width/6);
	    		for (Branch currentTree : treeArray) {
	    			if(dist(currentTree.x, height, posX, height) > 250){
	    				noTreeNear = true;
	    			} else {
	    				noTreeNear = false;
	    				break;
	    			}
	    		}
	    		
	    		compteurLoop++;
	    		if(compteurLoop > 500){
	    			noTreeNear = true;
	    		}
	    	}
	    }
    	createNewTree(posX);
    }

    int compteurArbre = 0;
	for (Iterator<Branch> iterator = treeArray.iterator(); iterator.hasNext(); ) {
	    Branch tree = iterator.next();
	    compteurArbre++;
	    if(tree.opacity > 0){
	    	tree.windForce = sin(windAngle) * 0.02;

	    	if(tree.opacity > 255-(compteurArbre*30)){
	    		tree.opacity-=5;
	    	} 

	    	tree.update(tree.opacity);
	    	tree.render();
		} else {
			iterator.remove();
		}
	}

	if(activateRain){
	  	if(maxRain < 350){
		  maxRain+=0.5;
        }

        if(backgroundColorHue > 60.0){
            backgroundColorHue-=0.5;
        }
	} else {
		if(maxRain > 0){
		  maxRain-=1.0;
        }

        if(backgroundColorHue < 180.0){
            backgroundColorHue+=0.5;
        }
	}

	sendOSCMessage("/eau", maxRain);

	if (rain.size()<floor(maxRain)){
		rain.add(new Rain());
		float reseed = random(0,.05);
		current = millis();
	}

	for (int i=0 ; i<rain.size() ; i++){
		Rain rainT = (Rain) rain.get(i);
		rainT.calculate();
		rainT.draw();

		if (rainT.position.y>height){

			for (int k = 0 ; k<random(1,4) ; k++){
				splash.add(new Splash(rainT.position.x,height));
			}

			rain.remove(i);
			float rand = random(0,100);
			if (rand>10&&rain.size()<floor(maxRain))
				rain.add(new Rain());
		}
	}

	for (int i=0 ; i<splash.size() ; i++){
		Splash spl = (Splash) splash.get(i);
		spl.calculate();
		spl.draw();
		if (spl.position.y>height)
		splash.remove(i);
	}

	
}

///////////////////////////////////////////////////////////
// Create new tree ////////////////////////////////////////
///////////////////////////////////////////////////////////
void createNewTree(float newPosX) {
    minX = width/2;
    maxX = width/2;
    minY = height;
    maxY = height;
    tree = new Branch(null, newPosX, height, PI, 110);
    float xSize = maxX-minX;
    float ySize = maxY-minY;
    float scale = 1;
    if(xSize > ySize) {
        if(xSize > 500)
            scale = 500/xSize;
    } else {
        if(ySize > 500)
            scale = 500/ySize;
    }
    tree.setScale(scale);
    tree.x = newPosX - xSize/2*scale + (tree.x-minX)*scale;
    //tree.y = height/2 + ySize/2*scale + (tree.y-maxY)*scale;
    tree.y = height;

    treeArray.add(0, tree);
}
  

// Fonction qui est appellée quand on recoit des donnees des ports serie:
void serialEvent(Serial p) {

    // Lire le message.
    String chaine = p.readString();

    // Separer les elements du message
    // selon les espaces:
    String[] elements = splitTokens(chaine);

    // S'assurer qu'il y a bien deux mots
    // dans le message et les appliquer aux variables :
    if ( elements.length == 2 ) {
        messageFirstElement = elements[0];
        messageSecondElement = int(elements[1]);

        // On peut "router" les messages en comparant le premier élément :
        if ( messageFirstElement.equals("wind") ){

            timeBetweenWindValues = millis();

        	valueWind = messageSecondElement;

            if(millis() - calibrateMillisWind < 5000 ){

            	if(recalibrateWind){
            		calibMinWind = 1023;
					calibMaxWind = 0;
					recalibrateWind = false;
            	}

                ready = false;

                if(valueWind < calibMinWind){
                    calibMinWind = valueWind;
                }

                if(valueWind > calibMaxWind){
                    calibMaxWind = valueWind;
                }

            } else {
                valueWind = map(valueWind, calibMinWind, calibMaxWind, 0, 50);
                if(valueWind < 0) valueWind = 0;

                int samples = 10;
                lopWind = lopWind + ((valueWind - lopWind)/samples) + defaultWindSpeed;
                constrain(lopWind, 0, calibMaxWind);

			    //sendOSCMessage("/wind", lopWind);
                //sendOSCMessage("/windmax", calibMaxWind);
                ready = true;
            }            
        }

        if ( messageFirstElement.equals("pho") ){
        	valuePhoto = messageSecondElement;

            if(millis() - calibrateMillisPhoto < 5000 ){

            	if(recalibratePhoto){
            		calibMinPhoto = 1023;
					calibMaxPhoto = 0;
					recalibratePhoto = false;
            	}

                ready = false;

                if(valuePhoto < calibMinPhoto){
                    calibMinPhoto = valuePhoto;
                }

                if(valuePhoto > calibMaxPhoto){
                    calibMaxPhoto = valuePhoto;
                }
            } else {
                valuePhoto = map(valuePhoto, calibMinPhoto, calibMaxPhoto, 0, photoTargetMaxVal);
                lopPhoto = previousLopPhoto + (lopPhotoAmount * (valuePhoto - previousLopPhoto));
		        previousLopPhoto = lopPhoto;
		        photoValReal = photoTargetMaxVal-lopPhoto;

		        //sendOSCMessage("/photo", photoValReal);
                ready = true;
            }            
        }

        if ( messageFirstElement.equals("eau") ){
        	valueEau = messageSecondElement;

            if(millis() - calibrateMillisEau < 5000 ){

                if(recalibrateEau){
                    calibMinEau = 1023;
                    calibMaxEau = 0;
                    recalibrateEau = false;
                }

                ready = false;

                if(valueEau < calibMinEau){
                    calibMinEau = valueEau;
                }

                if(valueEau > calibMaxEau){
                    calibMaxEau = valueEau;
                }
            } else {
                valueEau = map(valueEau, calibMinEau, calibMaxEau, 0, eauTargetMaxVal);

                if(valueEau > tresholdEau){
                    activateRain = true;
                } else {
                    activateRain = false;
                }

                ready = true;
            }            
        }

        if ( messageFirstElement.equals("terre") ){
        	valueTerre = messageSecondElement;

            if(valueTerre == 1){
            	doCreateNewTree = true;
            	//sendOSCMessage("/terre", 1);
            }         
        }
    }
}


// Fonction qui va chercher la geolocation actuelle et ensuite va chercher les informations sur la météo,
// On va chercher la direction du vent et sa vitesse. La direction du vent est utilisé pour dirigé la pluie et les arbres
// lorsqu'on souffle sur l'anémomètre et qu'on crée du vent. La vitesse de base n'est pas utilisée pour le moment.
void getCurrentWind(){
	try {

		JSONObject json;
		json = loadJSONObject("http://api.hostip.info/get_json.php");

		String countryCode = json.getString("country_code");
		String city = json.getString("city");
		
		try {

			JSONObject weather = loadJSONObject("http://api.openweathermap.org/data/2.5/weather?q="+city+","+countryCode+"&APPID=288d3a38f8c62817ba2409adbbf83b17");
			JSONObject windData = weather.getJSONObject("wind");
			defaultWindSpeed = windData.getFloat("speed"); 
			defaultWindDirection = windData.getFloat("deg");			
		} catch (Exception e) {
			println("error weather");
		}

	} catch (Exception e) {
		println("error city");
	}
}


// En appuyant sur la touche 1, un timer de 5 secondes permet de calibrer l'anémomètre
// la touche 2 calibre la photo résistance
// la touche 3 calibre l'eau
void keyPressed() {  

  if(key == '1'){
    calibrateMillisWind = millis();
    recalibrateWind = true;
  } else if(key == '2'){
    calibrateMillisPhoto = millis();
    recalibratePhoto = true;
  } else if(key == '3'){
    calibrateMillisEau = millis();
    recalibrateEau = true;
  }
   
}

// Permet de lancer le sketch en plein écran
boolean sketchFullScreen(){
  return true;
}


// Fonction qui permet d'envoyer des messages OSC
void sendOSCMessage(String messageID, float value){
    oscMessage = new OscMessage(messageID);
  	oscMessage.add(value);
  	oscP5.send(oscMessage, remoteLocationOSC);
}


// Création de fonction personnalisées pour aller chercher les informations de couleurs
// Pour que ce soit plus rapide que les fonctions de base de processing.
// Retourne la valeur d'alpha d'une couleur passée en paramètre.
float getAlpha(color c){
  colorMode(RGB);
  return ((c >> 24) & 0xFF);
}

// Retourne la valeur de rouge d'une couleur passée en paramètre.
float getRed(color c){
  colorMode(RGB);
  return ((c >> 16) & 0xFF);
}

// Retourne la valeur de vert d'une couleur passée en paramètre.
float getGreen(color c){
  colorMode(RGB);
  return ((c >> 8) & 0xFF);
}

// Retourne la valeur de bleu d'une couleur passée en paramètre.
float getBlue(color c){
  colorMode(RGB);
  return (c & 0xFF);
}

// Méthode qui retourne la couleur inverse de la couleur passée en paramètre
int inverseColor(int c){
	float R = getRed(c);
	float G = getGreen(c);
	float B = getBlue(c);
	color invserse = color(255-R, 255-G, 255-B);
	return invserse;
}

