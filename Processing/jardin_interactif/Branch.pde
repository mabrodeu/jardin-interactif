/*

Le code suivant est basé sur l'excellent code de Poersch, sur openprocessing.
Le code a été modifié entre autre pour avoir la notion de vent, à partir des données de l'Arduino.
Le visuel des arbres a été aussi modifié, mais la logique à la base de la génération des arbres revient à l'auteur du code original.

License originale:

"Recursive Tree" by Poersch, licensed under Creative Commons Attribution-Share Alike 3.0 and GNU GPL license.
Work: http://openprocessing.org/visuals/?visualID=90192    
License: 
http://creativecommons.org/licenses/by-sa/3.0/
http://creativecommons.org/licenses/GPL/2.0/

*/



///////////////////////////////////////////////////////////
// Class that handles the branches ////////////////////////
///////////////////////////////////////////////////////////
class Branch {
       
       
    ///////////////////////////////////////////////////////////
    // Variable definitions ///////////////////////////////////
    ///////////////////////////////////////////////////////////
    float x;
    float y;
    float angle;
    float angleOffset;
    float length;
    float growth = 0;
    float maxGrowth = 0;
    float windForce = 0;
    float blastForce = 0;
    int opacity = 255;
    Branch branchA;
    Branch branchB;
    Branch parent;
      
      
    ///////////////////////////////////////////////////////////
    // Constructor ////////////////////////////////////////////
    ///////////////////////////////////////////////////////////
    Branch(Branch parent, float x, float y, float angleOffset, float length) {
        this.parent = parent;
        this.x = x;
        this.y = y;
        if(parent != null) {
            angle = parent.angle+angleOffset;
            this.angleOffset = angleOffset;
        } else {
            angle = angleOffset;
            this.angleOffset = -0.2+random(0.4);
        }
        this.length = length;
        float xB = x + sin(angle) * length;
        float yB = y + cos(angle) * length;
        if(length > 22) {
            if(length+random(length*10) > 30)
                branchA = new Branch(this, xB, yB, -0.1-random(0.4) + ((angle % TWO_PI) > PI ? -1/length : +1/length), length*(0.6+random(0.3)));
            if(length+random(length*10) > 30)
                branchB = new Branch(this, xB, yB, 0.1+random(0.4) + ((angle % TWO_PI) > PI ? -1/length : +1/length), length*(0.6+random(0.3)));
            if(branchB != null && branchA == null) {
                branchA = branchB;
                branchB = null;
            }
        }
        minX = min(xB, minX);
        maxX = max(xB, maxX);
        minY = min(yB, minY);
        maxY = max(yB, maxY);

        maxGrowth = random(1, 1.75);
    }
      
      
    ///////////////////////////////////////////////////////////
    // Set scale //////////////////////////////////////////////
    ///////////////////////////////////////////////////////////
    void setScale(float scale) {
        length *= scale;
        if(branchA != null) {
            branchA.setScale(scale);
            if(branchB != null)
                branchB.setScale(scale);
        }
    }
      
      
    ///////////////////////////////////////////////////////////
    // Update /////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////
    void update(int opacity) {
        this.opacity = opacity;

        if(parent != null) {
            x = parent.x + sin(parent.angle) * parent.length * parent.growth;
            y = parent.y + cos(parent.angle) * parent.length * parent.growth;
            windForce = parent.windForce * (1.0+5.0/length) + blastForce*maxGrowth;
            blastForce = (blastForce + sin(x/2+windAngle)*0.015/length) * 0.98;
            //blastForce = (blastForce + sin(x/2+windAngle)*0.15/length) * 0.98;
            angle = parent.angle + angleOffset + windForce + blastForce;
            growth = min(growth + 0.1*parent.growth, maxGrowth);
        } else
            growth = min(growth + 0.1, maxGrowth);
        if(branchA != null) {
            branchA.update(this.opacity);
            if(branchB != null)
                branchB.update(this.opacity);
        }
    }
       
       
    ///////////////////////////////////////////////////////////
    // Render /////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////
    void render() {

        if(branchA != null) {
            float xB = x;
            float yB = y;
            if(parent != null) {
                xB += (x-parent.x) * 0.4;
                yB += (y-parent.y) * 0.4;
            } else {
                Ani.to(this, 0.5, "xB", (sin(angle+angleOffset) + windDirection) * length * 0.3);
                Ani.to(this, 0.5, "yB", (cos(angle+angleOffset) + windDirection) * length * 0.3);
                
                //xB += (sin(angle+angleOffset) - windDirection) * length * 0.3;
                //yB += (cos(angle+angleOffset) - windDirection) * length * 0.3;
            }

            stroke(lopPhoto, this.opacity);
            strokeWeight(length/5);
            noFill();
            beginShape();
            vertex(x, y);
            bezierVertex(xB, yB, xB, yB, branchA.x, branchA.y);
            endShape();
            
            branchA.render();
            if(branchB != null)
                branchB.render();
        }

    }    
}