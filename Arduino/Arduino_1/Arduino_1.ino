#include <Chrono.h>
#include <Bounce2.h>

#define TERRE_PIN 7
#define PHOTO_PIN 1
#define WIND_PIN 0

Bounce debouncer = Bounce(); 

Chrono envoiMessage;
int seed;
int terreMesurePrecedente;
int phoMesurePrecedente;
int windMesurePrecedente;
int eauMesurePrecedente;

void setup() {
  
    Serial.begin(57600);

    seed = analogRead(0) + analogRead(1) + analogRead(2) + analogRead(3) + analogRead(4) + analogRead(5);
    randomSeed( seed );

    pinMode(TERRE_PIN,INPUT_PULLUP);
    debouncer.attach(TERRE_PIN);
    debouncer.interval(5); // interval in ms
        
}

void loop() {

    debouncer.update();

    if ( debouncer.fell() ) {
        Serial.print("terre ");
        Serial.print(1);
        Serial.println();
    }

    if ( envoiMessage.metro(20) ) {
        // Mesurer la tension a la broche analogique 1 :
        int phoNouvelleMesure = analogRead(PHOTO_PIN);
        // Comparer la nouvelleMesure avec la precedente :
        if ( phoMesurePrecedente != phoNouvelleMesure ) {
            // Enregistrer la nouvelleMesure :
            phoMesurePrecedente = phoNouvelleMesure;
            Serial.print("pho ");
            Serial.print(phoNouvelleMesure);
            Serial.println();
        }
    }
    
    if ( envoiMessage.metro(20) ) {

        // Mesurer la tension a la broche analogique 1 :
        int windNouvelleMesure = analogRead(WIND_PIN);
        

        // Comparer la nouvelleMesure avec la precedente :
        if ( windMesurePrecedente != windNouvelleMesure ) {
                // Enregistrer la nouvelleMesure :
                windMesurePrecedente = windNouvelleMesure;

                // Envoyer la valeur du potentiometre.
                Serial.print("wind "); // "A1" suivi d'un espace
                Serial.print(windNouvelleMesure); // la valeur de la mesure
                Serial.println(); // indicateur de fin de ligne

        }

    }
}
