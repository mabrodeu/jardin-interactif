#include <CapacitiveSensor.h>

#define THRESHOLD 1000

CapacitiveSensor eau = CapacitiveSensor(3,2);
int seed;
int eauMesurePrecedente;

void setup() {
    
    Serial.begin(57600);

    //eau.set_CS_AutocaL_Millis(0xFFFFFFFF);
}

void loop() {

        // Mesurer la tension a la broche analogique 2 :
        long eauNouvelleMesure = eau.capacitiveSensor(30);
        // Comparer la nouvelleMesure avec la precedente :
        if ( eauMesurePrecedente != eauNouvelleMesure ) {
            Serial.print("eau ");
            Serial.print(eauMesurePrecedente);
            Serial.println();
            // Enregistrer la nouvelleMesure :
            eauMesurePrecedente = eauNouvelleMesure;
            /*if(eauNouvelleMesure < THRESHOLD){
              Serial.print("stop rain");
              Serial.println();
            } else {
              Serial.print("make it rain");
              Serial.println();
            }*/
            
        }
}
