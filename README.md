# jardin-interactif
## Par Pier Olivier Bourgault et [Marc-Antoine Brodeur](https://github.com/mabrodeur)

### Concept

Le but est de créer une façon d’interagir avec un paysage autant visuel que sonore par le biais de 4 capteurs faits maison : un anémomètre, un bouton, un verre d’eau ainsi qu’une photorésistance. Le projet se veut ludique et enfantin dans sa présentation, avec des interactions simples qui ont des effets concrets et instantanés sur la projection et l’ambiance sonore.
Ce concept se définit en un « jardin interactif » avec lequel il est possible de faire pousser des arbres, de faire pleuvoir, de changer la luminosité ainsi que d’imposer la force du vent sur tout le reste. Il est important pour le projet de jouer dans le concret et c’est pourquoi nous incorporons de touché de l’eau et de la terre pour opposer l’utilisation moins physique du souffle et de l’ouverture d’un coffret.

##Pour lancer le projet, il faut :
1. Brancher les 2 Arduino Leonardo à l'ordinateur.
2. Lancer le patch Max en mode présentation et réduire.
3. Dans le haut du projet Processing, s'assurer que les bons ports pour les Arduinos sont entrés. Lancer le projet Processing.
4. Effectuer la calibration de l'anémomètre, du coffret et du verre d'eau avec les touches 1, 2 et 3 du clavier, respectivement. (La calibration dure 5 secondes, pendant se temps il faut intéragir avec les items afin de leur fournir leur valeurs minimales et maximales selon le contexte).
6. Amusez-vous!

##Interactivité
###Jardin Interactif dépend de quatre interactivités :
- Faire tourner l’anémomètre fait lever le vent dans l’animation ainsi que dans la trame sonore; les arbres et la pluie changent de trajectoire pour le suivre. La force du vent dépend de l’intensité de la rotation de l’anémomètre.
- Toucher à l’eau fait pleuvoir; plus on touche à l’eau plus la pluie devient forte, autant en visuel qu’en sonorité.
- Appuyer sur le support en bois dans le pot de terre fait pousser des arbres.
- Ouvrir le coffre permet à la lumière de pénétrer dans l’animation, changeant les teintes de couleurs.

### Liste de Matériel

-	Moteur DC (1)
-	Bouton (1)
-	photorésistance
-	Tuyau de pvc
-	Cuillères à mesurer
-	Bouchon de bouteille (1)
-	Arduino Leonardo (2)
-	Plaques de styromousse
-	Câblage
-	Verres en carton/plastique (minimum 4)
-	Coffret
-	Peinture en aérosol
-	Bâtons « popsicle »
-	Terre
-	Colle chaude

### Objets

#### Anémomètre
- Démonter le cadre du moteur jusqu’à n’avoir que la partie principale du moteur rotative et la sortie de courant. 
- Insérer le moteur dans le tuyau de pvc et le fixer au besoin avec de la colle chaude.
- Fixer la partie rotative du moteur au bouchon.
- Coller les cuillères de côté sur le bouchon.
- Peinturer le tout.

#### Coffret photorésistant 
-	Percer le fond du coffret d’un petit trou.
-	Insérer la photorésistance  dans le trou. La résistance devrait tenir au fond du coffret et seules ses pattes devraient passer sous le coffret.

#### Verre d’eau interactif  
-	Percer un petit trou au fond du verre pour y passer un fil électrique.
-	Sceller le trou et fixer le fil avec de la colle chaude.
-	Découper le fond d’un verre pour faire un faux fond et y faire une incision pour laisser passer l’eau.
-	Coller le faux fond au-dessus du fil électrique.
-	Remplir le verre d’eau en s’assurant que le fil électrique entre en contact avec l’eau.

#### Planter un arbre  
-	Fixer le bouton au fond d’un verre de sorte que les pattes du bouton ressortent en-dessous du verre.
-	Fixer un double fond accoté sur le bouton.
-	Coller un bâton « popsicle » sur le faux fond juste au-dessus du bouton.
-	Remplir le verre de terre.

#### Base  
-	Couper la styromousse pour obtenir deux plaques de la forme désirée.
-	Creuser une des plaques de manière à créer un vide entre les deux plaques pour y insérer les arduinos et le câblage.
-	Fixer les deux plaques ensemble en les traversant de bâton « popsicle ».

#### Montage complété 
![Démo](https://raw.githubusercontent.com/mabrodeur/jardin-interactif/master/images/demo.gif)
![Montage complété](https://raw.githubusercontent.com/mabrodeur/jardin-interactif/master/images/montage_final.jpg)

### Schémas des connexions des Arduinos
![Arduino 1](https://raw.githubusercontent.com/mabrodeur/jardin-interactif/master/images/Arduino%201.png)
![Arduino 2](https://raw.githubusercontent.com/mabrodeur/jardin-interactif/master/images/Arduino%202.png)

### Crédits
- Inspiration du code Max/Msp et exemples de Patrice Coulombe et Kenny Lefebvre.
- Code Arduino et communication Serial basé sur le code et exemples de [Thomas Ouellet Fredericks](http://t-o-f.info) sur [le wiki du cours](http://wiki.t-o-f.info/EDM4640/EDM4640).
- Code de base pour la pluie : [Anastasis Chasandras](http://openprocessing.org/sketch/9299)
- Code de base pour les arbres : [Poersch](http://openprocessing.org/sketch/90192)

### License
Voir le fichier [LICENSE](https://raw.githubusercontent.com/mabrodeur/jardin-interactif/master/LICENSE)
